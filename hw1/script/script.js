// Программа должна уметь переводить 5 валют друг в друга (валюты на ваше усмотрение). 
// Пользователь вводит сначала название валюты, затем вводит сумму, и название валюты в которую хочет перевести, 
// после чего получает ответ (для общения с пользователем используем prompt и alert подробнее 
// [тут](https://learn.javascript.ru/alert-prompt-confirm)). 
// Если пользователь ввел что-то неверно, нужно повторить ввод этой информации. 
// По окончании конвертации спросить у пользователя хочет ли он произвести конвертацию еще раз. 
// Если да - то повторить все о'и. Если нет - выполнить выход из программы.

// Два варианта решения. Первое на основании лекций
// в переменные вынесены название валют, курсы.
// циклы do while проверяют входные данные и создают переменные для хранения данных. 
// Do while обвертка - запускает в случае подтверждения последующие обмены.
// реализовано для 5 валют обмен в обе стороны - курс не усредненный, а так называемый покупка продажа
// сравнение ввведенной валюты через if и подбор в какую меняем через switch case

// Второй вариант решен через объект для хранения курсов- сокращает количество переменных
// и три функции - функция проверки данных, функция запуска подсчета курса и повторного запуска
// третья функция для конвертации - без перебора использует особености ключей объекта
// легко маштабируется для дополнительный курсов

const USD = "USD";
const UAH = "UAH";
const GBP = "GBP";
const EUR = "EUR";
const CHF = "CHF";

const euroToUsd = 1.16;
const euroToGbp = 0.85;
const euroToUah = 30.51;
const euroToChf = 1.06;

const usdToEuro = 0.86;
const usdToUah = 26.30;
const usdToGbp = 0.89;
const usdToChf = 0.91;

const gbpToUsd = 1.36;
const gbpToEuro = 1.18;
const gbpToUah = 35.90;
const gbpToChf = 1.25;

const chfToUsd = 1.09;
const chfToGbp = 0.80;
const chfToUah = 28.80;
const chfToeuro = 0.94;

const uahToUsd = 0.038;
const uahToGbp = 0.028;
const uahToEuro = 0.033;
const uahToChf = 0.035;
let repeatExchange

do {
    let currencyCodeForExchange = "";
    let amountToExchange = 0;
    let currencyCodeExchangeTo = "";
    let amount = 0;

    do {
        currencyCodeForExchange = prompt ("enter currensy", `${UAH} or ${USD} or ${GBP} or ${CHF} or ${EUR}`)
    } while (
        currencyCodeForExchange.toLowerCase() !== UAH.toLowerCase() && 
        currencyCodeForExchange.toLowerCase() !== USD.toLowerCase() && 
        currencyCodeForExchange.toLowerCase() !== GBP.toLowerCase() && 
        currencyCodeForExchange.toLowerCase() !== EUR.toLowerCase() && 
        currencyCodeForExchange.toLowerCase() !== CHF.toLowerCase()
    );

    do {
        amountToExchange = +prompt("enter amount", "enter positive numbrer");
    } while (isNaN(amountToExchange) || amountToExchange < 1 || amountToExchange === "")
        
    do {
        currencyCodeExchangeTo = prompt ("enter currensy to exchenge", `${UAH} or ${USD} or ${GBP} or ${CHF} or ${EUR}`)
    } while (
        currencyCodeExchangeTo.toLowerCase() !== UAH.toLowerCase() && 
        currencyCodeExchangeTo.toLowerCase() !== USD.toLowerCase() && 
        currencyCodeExchangeTo.toLowerCase() !== GBP.toLowerCase() && 
        currencyCodeExchangeTo.toLowerCase() !== EUR.toLowerCase() && 
        currencyCodeExchangeTo.toLowerCase() !== CHF.toLowerCase()
    );    
    
    if (currencyCodeForExchange === USD) {
        switch (currencyCodeExchangeTo) {
            case EUR:
                amount = Math.round(amountToExchange * usdToEuro);
                break
            case UAH:
                 amount = Math.round(amountToExchange * usdToUah);
                break
            case GBP:
                amount = Math.round(amountToExchange * usdToGbp);
                break
            case CHF:
                amount = Math.round(amountToExchange * usdToChf);
                break
            default:
                amount = amountToExchange  
        }
    } else if (currencyCodeForExchange === EUR) {
        switch (currencyCodeExchangeTo) {
            case USD:
                amount = Math.round(amountToExchange * euroToUsd);
                break
            case UAH:
                 amount = Math.round(amountToExchange * euroToUah);
                break
            case GBP:
                amount = Math.round(amountToExchange * euroToGbp);
                break
            case CHF:
                amount = Math.round(amountToExchange * euroToChf);
                break
            default:
                amount = amountToExchange   
        }
    } else if (currencyCodeForExchange === UAH) {
        switch (currencyCodeExchangeTo) {
            case USD:
                amount = Math.round(amountToExchange * uahToUsd);
                break
            case EUR:
                 amount = Math.round(amountToExchange * uahToEuro);
                break
            case GBP:
                amount = Math.round(amountToExchange * uahToGbp);
                break
            case CHF:
                amount = Math.round(amountToExchange * uahToChf);
            default:
                amount = amountToExchange    
        }
    } else if (currencyCodeForExchange === GBP) {
        switch (currencyCodeExchangeTo) {
            case USD:
                amount = Math.round(amountToExchange * gbpToUsd);
                break
            case EUR:
                 amount = Math.round(amountToExchange * gbpToEuro);
                break
            case UAH:
                amount = Math.round(amountToExchange * gbpToUah);
                break
            case CHF:
                amount = Math.round(amountToExchange * gbpToChf);
            default:
                amount = amountToExchange 
        }
    } else if (currencyCodeForExchange === CHF) {
        switch (currencyCodeExchangeTo) {
            case USD:
                amount = Math.round(amountToExchange * chfToUsd);
                break
            case EUR:
                 amount = Math.round(amountToExchange * chfToeuro);
                break
            case UAH:
                amount = Math.round(amountToExchange * chfToUah);
                break
            case GBP:
                amount = Math.round(amountToExchange * chfToGbp);
            default:
                amount = amountToExchange  
        }
    }
    
    alert(`You will get ${amount} ${currencyCodeExchangeTo}`)
    repeatExchange = confirm("Произвести конвертацию еще раз?");
} while (repeatExchange);

// const currency = {
//     EUR: 
//     {
//     Usd: 1.16,
//     Gbp: 0.85,
//     Uah: 30.51,
//     Chf: 1.06,
//     },
//     USD: 
//     {
//         EUR: 0.86,
//     Uah: 26.30,
//     Gbp: 0.89,
//     Chf: 0.91,
//     },
//     GBP:
//     {
//     Usd: 1.36,
//     EUR: 1.18,
//     Uah: 35.90,
//     Chf: 1.25,
//     },
//     CHF:
//     {
//     Usd: 1.09,
//     Gbp: 0.80,
//     Uah: 28.80,
//     EUR: 0.94,
//     },
//     UAH:
//     {
//     Usd: 0.038,
//     Gbp: 0.028,
//     EUR: 0.033,
//     Chf: 0.035,
//     }
// }
// let currencyCodeForExchange = "";
// let amountToExchange = 0;
// let currencyCodeExchangeTo = "";
// let amount = 0;


// function getData(){
//     let repeatExchange = "";
//     do{
//         do {
//             currencyCodeForExchange = prompt ("enter currensy", `${UAH} or ${USD} or ${GBP} or ${CHF} or ${EUR}`)
//         } while (
//             validateCurrency(currencyCodeForExchange)
//         );
        
//         do {
//             amountToExchange = +prompt("enter amount", "enter positive numbrer");
//         } while (isNaN(amountToExchange) || amountToExchange < 1 || amountToExchange === "")
            
//         do {
//             currencyCodeExchangeTo = prompt ("enter currensy to exchenge", `${UAH} or ${USD} or ${GBP} or ${CHF} or ${EUR}`)
//         } while (
//             validateCurrency(currencyCodeExchangeTo)
//         );
//         const exchangeResult = convert({from: currencyCodeForExchange, to: currencyCodeExchangeTo, amountToExchange})
//         alert(`You will get ${exchangeResult} ${currencyCodeExchangeTo}`)
//         repeatExchange = confirm("Произвести конвертацию еще раз?");
//     } while (repeatExchange)
// }

// function convert (data) {
//     const {from, to, amountToExchange} = data;
//     const currencyRate = currency[from][to]
//     amount = amountToExchange*currencyRate
//     return amount
// }

// function validateCurrency (data){
//     return !Object.keys(currency).some(currencyName => currencyName === data)
// }
